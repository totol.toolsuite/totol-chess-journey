module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    colors: {
      'primary': '#A07178',
      'secondary': '#C8CC92',
      'dark': '#5A5353',
      'light': '#E6CCBE',
      'contrast': '#776274',
      'blue': '#1789FC',
      'danger': '#E67370',
      'success': '#64E38C'
    },
  },
  plugins: [],
}