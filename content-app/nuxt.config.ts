
// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  ssr: true,
  modules: [
    '@nuxt/content',
    '@nuxtjs/google-fonts',
  ],
  plugins: [
    { src: '~/plugins/apex-chart', mode: 'client' },
  ],
  googleFonts: {
    display: 'swap',
    preconnect: true,
    families: {
      'Poppins': [300, 400, 500, 600, 700],
    },
  },
  css: ['~/assets/css/main.scss', '@fortawesome/fontawesome-svg-core/styles.css'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
});
