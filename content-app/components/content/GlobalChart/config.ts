import { ApexOptions } from 'apexcharts';

export const chartOptions: ApexOptions = {
  chart: {
    id: "games-chart",
    animations: {
      enabled: true,
        easing: 'easeinout',
        speed: 1000,
        animateGradually: {
            enabled: true,
            delay: 150
        },
        dynamicAnimation: {
            enabled: true,
            speed: 350
        }
    },
    dropShadow: {
        enabled: true,
        top: 0,
        left: 0,
        blur: 1,
        color: '#000',
        opacity: 0.35,
    },
    fontFamily: 'Poppins',
    toolbar: {
      show: false
    },
    selection: {
      enabled: false
    },
    zoom: {
      enabled: false
    }
  },
  stroke: { curve: 'smooth' },
  markers: {
    size: [0, 10, 10, 10],
  },
  subtitle: {
    text: 'Classement ELO Fide',
    offsetY: 10,
  },
  tooltip: {
    followCursor: false,
  },
  xaxis: {
    type: 'datetime',
  }
}