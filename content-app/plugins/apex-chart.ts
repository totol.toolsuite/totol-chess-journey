import chart, { VueApexChartsComponent } from 'vue3-apexcharts'

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component('ApexChart', chart)
})