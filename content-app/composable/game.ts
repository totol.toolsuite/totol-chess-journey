import { ParsedContent } from "@nuxt/content/dist/runtime/types"
import { contentToIGame } from "~/models/game/game"
import { IGame } from "~/models/game/game.type"

export const useGames = async () => {
  const games = ref<ParsedContent[]>()
  games.value = (await queryContent('/games').find()).filter(x => x.pgn)


  const allGames =  computed(() => games.value?.map(game => (contentToIGame(game))) || [])

  const sortedAllGame = computed(
    () => allGames.value?.sort((game1: IGame, game2) => 
      game1.date.isAfter(game2.date) ? -1 : 1) || [])


  const gamesByTournament = (tournament: string): IGame[] => allGames.value.filter(x => x.tournament === tournament)

  return {
    allGames,
    sortedAllGame,
    gamesByTournament
  }
}