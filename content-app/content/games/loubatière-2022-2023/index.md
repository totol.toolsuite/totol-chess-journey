---
type: 'tournament'
tournament: 'Louabatière 2022-2023'
tournamentStartDate: '2023-01-25'
tournamentEndDate: '2023-01-25'
---
# Tournoi de la Louabatière

## Le tournoi

Mon équipe s'est qualifié au deuxième tour du tournoi sans moi. Mais suite à un absent, ils font appel à moi.

Le tournoi se déroule sur une journée, 3 parties, 7 équipes, 2 qualifiées.
3 rondes en 50min + 10s, une cadence assez rapide qui me plait beaucoup.

Notre équipe est celle qui a le elo moyen le plus faible de celles encore en lice.

J'arrive très fatigué car je sors d'un week-end avec les copains, mais je me suis engagé alors je suis présent à 9h pour en découdre.

J'arrive le matin et je découvre que mon elo est actualisé et est passé de 1130 à 1418, je suis ravis.
## Bilan 

Très en confiance après ma performance du matin, je suis content de ma prestation avec un 2/3.
Dommage que notre équipe soit éliminée mais la marche était trop haute et les écarts trop grands avec nos adversaires.