---
type: 'game'
date: '2023-03-26 13:00'
title: 'Ronde 2'
place: 'Saint-Brieuc'
cadence: '50m + 10s'
tags: ['KID']
tournament: 'Louabatière 2022-2023'
opponent: 'Lety'
my_color: 'black'
my_rate: '1418'
opponent_rating: '1373'
description: 'meta description of the page'
result: 0
pgn: '1. d4 g6 2. Nf3 Nf6 3. e3 Bg7 4. Bd3 d6 5. b3 O-O 6. Bb2 Nc6 7. Nbd2 Qe8 8. e4
e5 9. O-O Nh5 10. c4 exd4 11. a3 Nf4 12. Qc2 f5 13. Rfe1 Nxd3 14. Qxd3 Qd8 15.
Bxd4 Nxd4 16. Nxd4 c5 17. Nc2 Bxa1 18. Nxa1 Qf6 19. Nc2 b6 20. Ne3 Bb7 21. Nd5
Bxd5 22. Qxd5+ Qf7 23. Qd3 Rae8 24. f3 Re6 25. Qd5 Rf6 26. Qd3 f4 27. Nb1 g5 28.
Nc3 Rg6 29. h3 h5 30. Nd5 g4 31. e5 dxe5 32. Ne7+ Qxe7 33. Qxg6+ Qg7 34. Qxh5
gxf3 35. Qxf3 Rd8 36. Rd1 Rd4 37. Rxd4 cxd4 38. Qd5+ Kh7 39. Qe4+ Kh6 40. Qe2
Kg5 41. Qg4+ Kf6 42. Qh5 Qg5 43. Qh8+ Kf5 44. Qc8+ Ke4'
---

::GameHeader{title="Nulle très décevante après avoir été totalement gagnant"}
::

## Contexte

Deuxième partie de la journée, après manger ma gueule de bois commence à être difficile à gérer.
Je joue contre un adversaire de Saint Brieuc que j'avais déjà vu jouer contre un partenaire de club et il ne m'a pas semblé impressionnant.
À la table d'à côté se trouve un joeuur que j'avais battu [ronde 4 d'interclub 2023](championnat-n4-2023/ronde-4). Il dit à mon adversaire, attention il joue la Pirc, en rigolant.
## La partie

### Ouverture


Mon adversaire joue d4, surement pour ne pas jouer contre la Pirc. Tant pis, Kings Indian Defense. J'obtiens rapidement une meilleur position et même une pièce de plus.

### Milieu de jeu

Ma copine part de la salle, je me lève pour la racompagner sur le temps de mon adversaire, et lui dit confiant : "Celle là si je la perd, je paie le restau."

Le karma me rattrapera bientôt.
Bien qu'ayant une pièce de plus, je ne parviens pas à progresser avec mes tours tant la position est fermée.
Le cavalier de mon adversaire a presque plus de force relative qu'une tour, et fini par me mettre une fourchette.

### Finale

Mon adversaire obtient une position supérieur dans la finale, mais par déception, et tilt, il me propose la nulle et je la refuse.

J'essaie de forcer un petit peu le gain mais cela devient trop risqué avec les deux dames en jeu, et je ne parviens pas à le faire craquer.
Je finis par proposer nulle et il accepte.
## Bilan

Très décu de cette partie, après un très bon début je me suis déconcentré et ait laissé passer ma chance. Je sauve la nulle au moins.