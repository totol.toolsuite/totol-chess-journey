---
type: 'game'
date: '2023-03-26 17:00'
title: 'Ronde 3'
place: 'Saint-Brieuc'
cadence: '50m + 10s'
tags: ['KIA']
tournament: 'Louabatière 2022-2023'
opponent: 'Noel GUINARD'
my_color: 'white'
opponent_rating: '1537'
my_rate: '1418'
description: 'meta description of the page'
result: 0
pgn: '1. e4 c5 2. Nf3 e6 3. g3 Nc6 4. Bg2 Qc7 5. O-O Nf6 6. Re1 Be7 7. c3 a6 8. d3 b5
9. Bf4 Qb6 10. e5 Nd5 11. Be3 Qc7 12. d4 Nxe3 13. Rxe3 cxd4 14. Nxd4 Bb7 15.
Nxc6 Bxc6 16. Rd3 Rd8 17. Bxc6 dxc6 18. Rxd8+ Qxd8 19. Qxd8+ Kxd8 20. Nd2 *'
---

::GameHeader{title="Fatigué, on assure la nulle pour l'équipe"}
::
## Contexte

Une partie a duré très longtemps à la ronde précédente, je suis épuisé il est 17h, je n'ai qu'une envie c'est finir ma nuit.

Je joue contre un adversaire qui avait joué échiquier 1 avec Yffiniac, il avait perdu contre mon coequipier de club, mais je le sais très fort.

### Ouverture

1 e4 c5, c'est reparti, confiant après ma victoire du matin, j'envois Kings Indian Attack.
### Milieu de jeu

Mon adversaire joue très bien et ne me laisse pas trop d'opportunité d'attaque. Je ne prends aucun risque, en me disant qu'une partie nulle m'irait très bien. 
Sur 2 des 3 autres échiquiers de mes collègues, les positions sont très favorables.
Après une série d'échange qui rend la position totalement plate, je décide de proposer la nulle directement après seulement 20coups.
Mon adversaire qui a 150 points elo de plus que moi réfléchis 5 10 minutes, avec l'air agacé. 
Sa position est légèrement inférieur, refuser semble risqué, mais faire nulle contre moi est un mauvais résultat.

Il finit par me tendre la main et accepter. Nous avons ensuite analysé la position tous les deux, très sympa.

## Bilan

Gros manque de combativité de ma part par fatigue, mais cela reste un bon résultat pour moi. Première fois que je propose la nulle comme ça dans une position avec encore autant de pièces.