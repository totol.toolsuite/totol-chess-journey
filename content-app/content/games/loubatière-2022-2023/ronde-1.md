---
type: 'game'
date: '2023-03-26 9:00'
place: 'Saint-Brieuc'
tournament: 'Louabatière 2022-2023'
title: 'Ronde 1'
tags: ['KIA', 'Zeitnot']
cadence: '50m + 10s'
my_rate: '1418'
my_color: 'white'
opponent: 'Malo GLEMEE'
opponent_rating: '1640'
description: 'meta description of the page'
result: 1
lichess: 'https://lichess.org/study/PsE7KLQl/lkqLxM3q'
pgn: '
1. e4 c5 2. Nf3 Nc6 3. g3 g6 4. Bg2 d6 5. O-O Bg7 6. c3 e5 7. d3 Nge7 8. Re1 O-O 9. Nbd2 h6 10. a4 f5 11. b4 Qe8 12. Rb1 cxb4 13. cxb4 Be6 14. b5 Nd8 15. Ba3 Nf7 16. Nc4 Rd8 17. b6 a6 18. a5 d5 19. exd5 Rxd5 20. d4 e4 21. Bf1 Qd7 22. Nh4 Rxd4 23. Qc2 Rc8 24. Red1 Nc6 25. Qa4 Kh7 26. Rxd4 Nxd4 27. Qxd7 Bxd7 28. Rd1 Be6 29. Ne3 Ng5 30. Kg2 Bb3 31. Rc1 Rxc1 32. Bxc1 Ndf3 33. Bc4 Bxc4 34. Nxc4 Bc3 35. Nd6 Bxa5 36. Be3 Ne1+ 37. Kh1 Nc2 38. Nxb7 Nxe3 39. Nxa5 Nd5 40. b7 '
---
::GameHeader{title="Mon adversaire craque dans le Zeitnot"}
::
## Contexte

Mon Elo est mis à jour suite à l'open d'Avoine, je joue à 1418, et c'est partie pour 3 rondes de 50m + 10s.
Changement d'heure + soirée la veille, j'ai dormi 3h, c'est pas la grosse forme mais je suis motivé

## La partie

### Ouverture

Depuis quelque semaine j'essaye de travailler la Kings Indian Attack, pour l'ajouter à mon répertoire contre sicilienne et française.
Cette ouverture a l'avantage d'avoir beaucoups de thêmes et tactiques en commun avec la Kings Indian Defense et la Pirc que je joue avec les noirs.
1 e4 c5, je me lance et je le joue pour la première fois.


### Milieu de jeu

Je sors de l'ouverture avec une belle position et des clouages intéressants, mais petit à petit mon adversaire me tricote et réussi à s'en sortir et reprendre l'avantage
### Finale

Les 3 autres parties de mes collègues sont terminées et toutes perdues, nous ne jouons plus pour rien au niveau de l'équipe.
Mais je reste très concentré, nous sommes en Zeitnot avec 2minutes chacun et seulement 10 secondes de rajout par coup.
Bien qu'en difficulté à la position, mon adversaire a un pion de plus et des cavalier très actifs mais je vois que son attaque ne menace rien.
Je décide de créer des complications et de jouer très vite.
Mon adversaire pris de cours par le temps avec quelques secondes rentre dans une mauvaise série d'échange et ne peut plus arrêter mon pion b. 
Il arrête la pendule et me tend la main.
## Bilan

Très heureux de cette victoire, à 1640 elo, c'est pour l'instant ma plus belle victoire. Bien qu'inférieur dans le milieu de jeu, je me suis accroché et ait fait preuve de sang froid dans la finale et le zeitnot