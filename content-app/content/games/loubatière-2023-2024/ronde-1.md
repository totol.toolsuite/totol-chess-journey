---
type: 'game'
date: '2023-11-11 9:00'
place: 'Landebia'
tournament: 'Louabatière 2023-2024'
title: 'Ronde 1'
tags: ['Pirc']
cadence: '50m + 10s'
my_rate: '1453'
my_color: 'black'
opponent: 'Thomas Plouzan'
opponent_rating: '1199'
description: 'meta description of the page'
result: 1
lichess: 'https://www.chess.com/analysis/game/pgn/4PGNLcHnX8?tab=analysis&move=28'
pgn: '
1. e4 g6 2. Nf3 d6 3. d4 Nf6 4. Nc3 Bg7 5. Bd3 O-O 6. Bg5 c6 7. Qe2 Bg4 8. h3
Bxf3 9. Qxf3 Nbd7 10. h4 b5 11. a3 a5 12. Ne2 Qb6 13. O-O-O a4 14. h5 b4 15.
axb4 $2 Qxb4 16. hxg6 hxg6 17. Qh3 a3 18. bxa3 Rxa3 19. c3 Qa5 20. Kd2 Qxg5+ 21.
f4 Qh5 22. Qg3 Qg4 23. Qh2 Rb8 24. e5 Nh5 25. Ke1 Rb2 26. f5 Raa2 27. g3 Nxg3
28. Qh7+ Kf8 29. Rh2 Rxe2+ 30. Bxe2 Rxe2+ 31. Rxe2 Qxe2#'
---
::GameHeader{title="Dernier échiquier de l'équipe 1, on attend de moi des victoires"}
::
## Contexte

C'est repartit pour la coupe Loubatière, mais cette fois je suis en forme, et au dernier échiquier dans une équipe plutôt forte, alors que la dernière fois j'étais premier échiquier dans une équipe plus faible en classsement.

C'est parti pour une journée à 3 parties, j'affronte en premier un jeune d'un 15aine d'années, non classé. Attention ces profils sont toujours dangereux.

## La partie

### Ouverture

Je joue ma PIRC contre e4 comme à l'habitude. Mon adversaire sort rapidement de la ligne principale. Et perd quelques temps dans l'ouverture par conséquent.


### Milieu de jeu

Mon adversaire me surprend beaucoup avec un grand roque, je suis persuadé sur le moment d'être largement en avance, l'ordinateur ne confirme pas et dit qu'avec les bons coups les blancs pouvaient tenir.
Malgré tout, j'envoi mon attaque sur son roi qui arrive avant la sienne. Cela ira jusqu'au mat.

## Bilan

Bonne entrée en matière, très à l'aise avec ma PIRC, j'ai assez confiance.