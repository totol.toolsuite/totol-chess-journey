---
type: 'game'
date: '2023-11-11 13:00'
place: 'Landebia'
tournament: 'Louabatière 2023-2024'
title: 'Ronde 2'
tags: ['KIA']
cadence: '50m + 10s'
my_rate: '1453'
my_color: 'white'
opponent: 'Mosan Mc Kee'
opponent_rating: '1078'
description: 'meta description of the page'
result: 1
lichess: 'https://www.chess.com/analysis/game/pgn/3XZdfGc4ke?tab=review'
pgn: '
1. Nf3 c6 2. g3 Nf6 3. Bg2 g6 4. O-O Bg7 5. d3 O-O 6. e4 d6 7. Bg5 Nbd7 8. Nbd2
Nc5 9. Re1 h6 10. Be3 Ng4 11. Bxc5 dxc5 12. h3 Nf6 13. Nf1 Nd7 14. c3 b6 15. e5
Rb8 16. N1h2 Bb7 17. Qd2 Qc7 18. Qe3 Rbe8 19. Re2 Kh8 20. h4 e6 21. Rae1 Re7 22.
Qf4 f6 23. exf6 Qxf4 24. fxg7+ Kxg7 25. gxf4 Rxf4 26. Rxe6 Rxe6 27. Rxe6 Kf7 28.
Bh3 Nf6 29. Re3 Nd5 30. Be6+ Kf8 31. Re4 Rxe4 32. dxe4 Nf4 33. Bd7 Nd3 34. b3
Ke7 35. Bh3 a5 36. Ng4 h5 37. Nge5 Nxe5 38. Nxe5 b5 39. f4 b4 40. c4 Ba8 41.
Nxg6+ Kf6 42. Ne5 Bb7 43. Kf2 Ba8 44. Ke3 Bb7 45. Bd7 Ke7 46. Bxc6 Bxc6 47.
Nxc6+
'
---
::GameHeader{title="Ronde 2, on confirme la bonne entrée dans le tournoi"}
::
## Contexte

Deuxième ronde, je joue la dernière table d'Yffiniac, j'avais fait nulle avec leur première table l'année dernière, je suis donc un peu en confiance

## La partie

### Ouverture

Je décide de rentrer dans du jeu positionnel avec la KIA, pour la pratiquer le plus possible. En plus c'est ce que je joue à fond en ce moment en ligne


### Milieu de jeu

Je parviens à progresser petit à petit grâce à mon pion e5, et mon adversaire a des pions doublés. Je passe à côté de la poussée e6 qui fonctionnait bien, à laquelle je n'ai pas cru. Je l'avais vu mais je trouvais ça très compliqué pour pas grand chose. Finalement celà permettait de rentrer au prix d'un pion et donnait un avantage de +2

### Finale

Avec une tactique et un échec intermédiaire je finis par obtenir une qualité. Ensuite j'ai liquidé et terminé proprement. L'ordinateur voit des attaques directement sur le roi mais j'ai préféré finir sans prendre de risque

## Bilan

Solide, bonne parte, j'ai eu l'avantage tout le long comme à la première partie.