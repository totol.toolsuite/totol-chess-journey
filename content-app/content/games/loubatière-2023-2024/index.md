---
type: 'tournament'
tournament: 'Louabatière 2023-2024'
tournamentStartDate: '2023-11-11'
tournamentEndDate: '2023-11-11'
---
# Tournoi de la Louabatière 2023-2024

## Les qualifs

Réservé aux joueurs de moins de 1700 elo, cette coupe permet aux joueurs amateurs de jouer compétitivement. Je joue dernier échiquier de l'équipe 1 du club. Notre équipe est une des favorites car nous avons des 1450 + à tous les échiquiers. Tout du moins pour les qualifs, aux prochains tour ce risque d'être plus compliqué

## Bilan des qualifs.

Avec 10,5/12, et 2,5 sur 3 de mon côté, mon équipe termine première du classement et se qualifie donc facilement. Objectif rempli