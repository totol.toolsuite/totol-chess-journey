---
type: 'game'
date: '2023-11-11 16:00'
place: 'Landebia'
tournament: 'Louabatière 2023-2024'
title: 'Ronde 3'
tags: ['PIRC']
cadence: '50m + 10s'
my_rate: '1453'
my_color: 'black'
opponent: 'Placier'
opponent_rating: '1155'
description: 'meta description of the page'
result: 0
lichess: 'https://www.chess.com/analysis/game/pgn/4PGNLcHnX8?tab=analysis&move=28'
pgn: '
1. e4 d6 2. d4 Nf6 3. Nc3 g6 4. Nf3 Bg7 5. Be2 O-O 6. O-O e5 7. dxe5 dxe5 8.
Qxd8 Rxd8 9. Bg5 Re8 10. Nd5 Nxd5 11. exd5 c5 12. Bb5 Bd7 13. Bxd7 Nxd7 14. c3
b5 15. Nd2 Nb6 16. Ne4 Nxd5 17. Nxc5 h6 18. Bh4 f5 19. Rad1 Nb6 20. f4 e4 21.
Nd7 Nxd7 22. Rxd7 e3 23. Re1 Re4 24. g3 b4 25. cxb4 Bxb2 26. Re7 Re8 27. Rxe8+
Rxe8 28. Kg2 Bc3 29. Re2 Bxb4 30. Bf6 Bd2 31. Bd4 a6 32. Kf3 Kf7 33. Bxe3 Bxe3
34. Rxe3 Rxe3+ 35. Kxe3 Ke6 36. Kd4 Kd6
'
---
::GameHeader{title="Ronde 3, je suis fatigué mais on essaie d'aller chercher le 3/3, mais cela se solde par une nulle"}
::
## Contexte

Dernière ronde, je suis assez fatigue, je me suis que c'est jouable car mon adversaire est à 1150.

## La partie

### Ouverture

Mon adversaire joue dans la main line de la PIRC. Je sors de l'ouverture en ayant égalisé car il choisit la variante d'echange en e4 e5.


### Milieu de jeu

J'obtiens un très très légère avantage car un pion passé. Je ne parviens pas à concrétiser.

### Finale

Au final il s'en sort avec une meilleure finale, je propose nulle et suis soulagé quand il accepte.

## Bilan

On sauve les meubles, j'ai pas réussit à presser, dommage.