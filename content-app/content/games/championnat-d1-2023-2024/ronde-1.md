---
type: 'game'
date: '2023-11-12'
place: 'Landebia'
tournament: 'Interclubs 2023-2024'
title: 'Ronde 1'
tags: ['KIA']
cadence: '1h30 + 30s'
my_rate: '1453'
my_color: 'white'
opponent: 'Inconnu'
opponent_rating: '1155'
description: 'meta description of the page'
result: 1
lichess: 'https://www.chess.com/analysis?tab=analysis'
pgn: '1. Nf3 Nc6 2. g3 e5 3. d3 d5 4. Nbd2 Bc5 5. Bg2 Nf6 6. O-O O-O 7. e4 d4 8. c3
dxc3 9. bxc3 Re8 10. Nb3 Bb6 11. Bg5 h6 12. Bxf6 Qxf6 13. d4 Bg4 14. d5 Rad8 15.
Nbd2 Ne7 16. Qc2 c6 17. h3 Bxf3 18. Bxf3 cxd5 19. Rad1 d4 20. cxd4 Bxd4 21. Bg4
Rd6 22. Qc7 Rb6 23. Nc4 $2 Rc6 24. Qd7 Kf8 $6 25. Rxd4 $6 exd4 26. e5 Qg6 $1 27. Nd6
Rb8 28. f4 $2 Qd3 29. Bf3 Qe3+ 30. Kh1 Rc1 $6 31. Rxc1 Qxc1+ 32. Kh2 Qc2+ $6 33. Bg2
d3 34. f5 $2 d2 $4 35. f6 $1 gxf6 $4 36. exf6 Qxa2 $6 37. fxe7+ $6 Kg8 $6 38. e8=Q+ Rxe8
39. Qxe8+ Kh7 40. Qe4+ Kg7 41. Qg4+ $6 Kf8 42. Qd1 Qb2 43. Ne4 Qc2 $6 44. Qxc2'
---
::GameHeader{title="Grosse frayeur"}
::

## Contexte

Supérieur sur la papier et en plus avec les blancs, je me dis que la partie devrait être reposante pour moi. Après avoir joué 3 parties longues la veille au tournoi de la loubatière, je suis plutôt d'humeur pour une partie posée, sans trop de complications

## La partie

### Ouverture

J'ouvre avec ma KIA habituelle, et commet une faute dans les premiers coups. Je passe mon temps à aller vapoter dehors, je ne suis pas dedans.

### Milieu de jeu

Le milieu de jeu se passe donc avec 1 pion et moins et ma position qui se détériore. Je décise de sac une tour pour prendre du jeu et essayer au moins de l'aggresser plutôt que de perdre à petit feu.

### Finale

La final part dans tous les sens, il est sur le point de faire une dame mais mon pion arrive avec menace de mat avec un temps de plus. Il a trop sous estimé ma menace, et je me retrouve avec une position gagnante que je convertis.

## Bilan

Je ne dois jamais autant me mettre en difficulté comme ça avec les pièces blanches contre un joueur avec 300 elos de moins que moi. J'ai pris beaucoup de risques mais au moins la partie était palpitante