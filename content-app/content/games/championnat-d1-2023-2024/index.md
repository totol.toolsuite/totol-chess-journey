---
type: 'tournament'
tournament: 'Interclubs 2023-2024'
tournamentStartDate: '2022-09-01'
tournamentEndDate: '2023-07-31'
---
# Matchs d'interclubs de la saison 2023/2024

## Contexte

2ème saisons, cette saison mon équipe présente 3 équipes, une n4, un d1, et une d2. L'objectif est de faire monter au moin la n4 et la d1.

Je suis 1450 en début d'année, l'objectif de la saison serait à titre personnel d'atteindre 1600, et pour l'équipe de d1 de monter en n4.

## Bilan

