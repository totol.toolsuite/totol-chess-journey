---
type: 'game'
date: '2024-01-28'
place: 'Saint-Brieuc'
tournament: 'Interclubs 2023-2024'
title: 'Ronde 4'
tags: ['KID']
cadence: '1h30 + 30s'
my_rate: '1448'
my_color: 'white'
opponent: 'Joueur de Saint Brieuc'
opponent_rating: '1199'
description: 'meta description of the page'
result: 0
lichess: 'https://lichess.org/study/pFWPIhrQ'
pgn: '1. d4 g6 2. Bf4 Nf6 3. Nc3 d6 4. h3 c6 5. e4 Nbd7 6. Nf3 b5 7. d5 b4 8. dxc6
bxc3 9. cxd7+ Bxd7 10. bxc3 Nxe4 11. Qd4 Nf6 12. Bc4 Bg7 13. Qe3 O-O 14. O-O Qc7
15. Bb3 Bc6 16. Nd4 Nd5 17. Qd2 Bb7 18. c4 Nxf4 19. Qxf4 e5 20. Nb5 Qc6 21. Qg3
a6 22. Nc3 e4 23. Nd5 Kh8 24. Rad1 Be5 25. Qh4 f5 26. Ne7 Qe8 27. Nd5 Bxd5 28.
Rxd5 Ra7 29. Rfd1 Re7 30. c5 Bf6 31. Qh6 dxc5 32. Ba4 Bg7 33. Qg5 Bf6 34. Qxf6+
Rxf6 35. Bxe8 Rxe8 36. Rxc5 Re7 37. Rc8+ Kg7'
---

::GameHeader{title="Je rate un gain en milieu de jeu"}
::

## Contexte

J'ai un peu délaissé les échecs pour le tennis depuis 2 3 mois, je ne prends pas le temps d'aller jouer au club le mardi, je ne joue plus en ligne. C'est donc pas vraiment en confiance que j'arrive à Saint-Brieuc, mais mon adversaire est moins bien classé donc je reste assez confiant.

## La partie

### Ouverture

Mon adversaire joue une sorte de Jobava London, et je répond avec la KID classique, et la variante d'extension à l'aile dame au cas ou il souhaite jouer le Roc opposé

### Milieu de jeu

Mon adversaire connait moins bien que moi et j'obtiens un léger avantage, et des tactiques sont dans l'air. Je pense en trouver une gagnante mais celà ne marche finalement pas. Je suis passé à côté d'une tactique pas trop compliquée, un peu deg.
J'étais sur que ça marchait pas, mon coéquipier Manu lui l'avait vu et il avait raison. Ce bougre

### Finale

J'ai raté ma chance, je me retrouve dans une position égale ou c'est plutôt moi qui risque. Je demande à mon capitaine si la win est importante, il me dit que non, je décide de ne pas m'envoyer en l'air et je propose la nulle plutôt que de prendre des risques, contre mon adversaire qui joue clairement mieux que 1199, donc je m'en méfie je peux perdre beaucoup de point sur une défaite contre lui.

## Bilan

Après être monté pas mal au classement l'année dernière, il est difficile pour moi de jouer à ce niveau sans aucun entrainement ou pratique. Je suis passé à côté du match. J'évite la défaite au moins, mais pas du tout satisfait de mon niveau de jeu.
