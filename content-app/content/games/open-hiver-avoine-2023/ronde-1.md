---
type: 'game'
date: '2023-02-22 13:00'
place: 'Avoine'
tournament: 'Open Hiver Avoine 2023'
title: 'Ronde 1'
tags: ['Pirc', 'Attaque Autrichienne']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'black'
opponent: 'Liv ROINEL'
opponent_rating: '1340'
description: 'meta description of the page'
result: 0
lichess: https://lichess.org/study/zjP8UWwS/Dr3RpoLO
pgn: '1. e4 d6 2. d4 Nf6 3. Nc3 g6 4. f4 Bg7 5. Nf3 c5 6. dxc5 Qa5 7. Bb5+ Bd7 8.
Bxd7+ Nbxd7 9. O-O Qxc5+ 10. Kh1 O-O 11. Qe1 Rfc8 12. f5 b5 13. a3 a5
14. Be3 Qc4 15. fxg6 hxg6 16. Bd2 e6 17. e5 Nxe5 18. Nxe5 dxe5 19. Qxe5 Nd5
20. Qe4 Qxe4 21. Nxe4 Rxc2 22. Rad1 b4 23. Nd6 f5 24. Rfe1 Ra6 25. Rxe6 Bf8 26.
Re8 Rxd6 27. Bh6 Ne3 28. Re1 Rd1 29. Rxd1 Nxd1 30. Rxf8+ Kh7 31. Bf4
Nxb2 32. axb4 Nd3 33. Bg5 axb4 34. h4 b3 35. Rb8 b2 36. Rb7+ Kg8 37. Rb8+
Kf7 38. Kh2 Rc4 39. Rb7+ Ke6 40. Rb6+ Kd5 41. Bf6 Rb4 42. Rxb4'

---
::GameHeader{title="Entrée en matière encourageante"}
::

## Contexte

Premier match de l'Open, nous sommes arrivés la veille pour être bien près.
Je suis appareillé contre une jeune joueuse classée 1340. Je suis classé 1130, mais je suis confiant.

## La partie

### Ouverture

J'ai préparé la défense Pirc pour le tournoi. Liv me joue 1. e4 et je suis donc très content.
Elle joue une attaque Autrichienne avec les pions e4 d4 et f4 qui font pression sur le centre.
J'étais plutôt préparé contre, et sors très bien de l'ouverture.

### Milieu de jeu

Je parviens à prendre un avantage tactique en sortie d'ouverture, et ait une très bonne position.
Elle parvient à créer des complications, et au coup 27, je réfléchis très longtemps pour me sortir de cette attaque.
Je finis par trouver une tactique pour obstruer le fou h6, avec 27...Ne3. Pas de chance, j'avais la bonne idée mais le bon coup était Nf4.
Au coup 31, Liv me propose la nulle.

### Finale
Je n'ai plus beaucoup de temps à la pendule, mais pense que je peux obtenir quelque chose dans cette position et refuse. Je suis venu ici pour jouer.
Malheureusement, je rate le gain avec 41...Rb4 au lieu de 41...Rc6 qui gagnait instantannément.
Je n'avais plus le temps, je me trompe et propose donc la nulle, il n'y a plus rien à jouer.

## Bilan

Plutôt bonne entrée en matière, je suis content de mon match, de mon ouverture, et de ma combativité, même si je suis très décu d'avoir raté le gain.