---
type: 'game'
date: '2023-02-24 13:00'
place: 'Avoine'
tournament: 'Open Hiver Avoine 2023'
title: 'Ronde 6'
tags: ['Alapine']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'white'
opponent: 'Alexandre PEIGNELIN'
opponent_rating: '1682'
description: 'meta description of the page'
result: -1
lichess: https://lichess.org/study/zjP8UWwS/GmntrOEB
pgn: '1. e4 c5 2. c3 Nc6 3. d4 cxd4 4. cxd4 e6 5. Nf3 d5 6. e5 Nge7 7. Bd3 Bd7 8. a3
g6 9. O-O Bg7 10. Nc3 a6 11. Bg5 O-O 12. Qd2 Qe8 13. Ne2 Nf5 14. Bc2 f6 15. exf6
Bxf6 16. Rac1 Qe7 17. Bxf5 Bxg5 18. Nxg5 Rxf5 19. f4 Raf8 20. Ng3 R5f6 21. h4
Qd6 22. h5 Rxf4 23. Ne2 Rxf1+ 24. Rxf1 Rxf1+ 25. Kxf1 gxh5 26. Nf4 Qf8 27. Kg1
Qf5 28. Nxh5 h6 29. g4 Qxg4+ *'

---
::GameHeader{title="Belle attaque mais manque de réalisme et craquage"}
::

## Contexte

Après mon [marathon gagnant du matin](/games/open-hiver-avoine-2023/ronde-5), j'affronte un adversaire proche de 1682.
Je n'ai pratiquement pas mangé par stress et manque de temps.

## La partie

### Ouverture

Mon adversaire répond par la sicilienne contre mon 1. e4, je suis déçu de ne toujours pas pouvoir jouer la Ponziani.
Mais j'ai un petit peu préparé l'Alekhine dans ce position, que je suis content de jouer.
J'obtiens une position qui ressemble à une française d'avance, qui me va très bien.

### Milieu de jeu

Je tente une attaque contre son roque, qui se passe tout d'abord très bien. Mon adversaire manoeuvre mais je sens que j'ai une opportunité.
Finalement dans le petit jeu il réussit à se sortir d'une position difficile. Je rate une tactique pourtant pas si difficile avec 29.Cf4, et m'incline.

## Bilan

Assez bonne partie pendant 3/4 de la partie mais je manque de concentration et ne parviens pas à capitaliser sur un pourtant bon avantage.
Ensuite assez dur de me remettre dans le match, et j'abandonne un peu trop facilement.