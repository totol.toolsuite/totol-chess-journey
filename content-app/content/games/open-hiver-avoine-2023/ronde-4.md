---
type: 'game'
date: '2023-02-23 13:00'
place: 'Avoine'
tournament: 'Open Hiver Avoine 2023'
title: 'Ronde 4'
tags: ['Alekhine']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'white'
opponent: 'G CANIAGDEL'
opponent_rating: '1560'
description: 'meta description of the page'
result: -1
lichess: https://lichess.org/study/zjP8UWwS/5vw25OOj
pgn: '1. e4 Nf6 2. d3 e5 3. Nf3 Nc6 4. c3 d5 5. Qb3 h6 6. Nbd2 Bc5 7. d4 exd4 8. cxd4
Bxd4 9. Nxd4 Nxd4 10. Qa4+ Nc6 11. e5 Qe7 12. Be2 Qxe5 13. Nf3 Qd6 14. O-O Qb4
15. Qc2 O-O 16. a3 Qd6 17. b4 Bg4 18. Bb2 Rfe8 19. Rad1 Rad8 20. b5 Na5 21. Rfe1
Bxf3 22. Bxf3 Rxe1+ 23. Rxe1 Nc4 24. Bc1 c6 25. bxc6 bxc6 26. Qa4 a5 27. Be2 Re8
28. f3 Qc5+ 29. Kf1 Ne3+ 30. Bxe3 Rxe3 31. Qf4 Qxa3 32. Kf2 Re6 33. Rb1 Qc5+ 34.
Kf1 Rxe2 35. Qb8+ Re8 '

---
::GameHeader{title="Gros tilt contre une Alekhine"}
::

## Contexte

Je mange au restaurant avec mon ami Paul, qui lui est à 0/3. Nous nous changeons les idées et buvons un coup.
Retour pour 14h, je suis motivé pour renouer avec la victoire.
Mon adversaire est classé 1560, je me dis que j'ai la possibilité de réaliser une bonne perf.

## La partie

### Ouverture

J'ai préparé la Ponziani pour ce tournoi avec :

-1. e4 e5 
-2. Cf3 Cc6 
-3. 3.c3 ...

J'envoi 1. e4, et là mauvaise nouvelle, 1... Cf6, l'Alekhine.
Je sais que la réponse la plus simple est de pousser e5, mais j'ai sur le moment envie de forcer pour jouer la ponziani ou une structure similaire.

J'essaie de m'accrocher en donnant un temps à mon ouverture, mais c'est une très très mauvaise décision.

### Milieu de jeu

Mon adversaire se retrouver à jouer avec les noirs avec une position de blancs.
Je me fais totalement dominer en milieu de partie, et concède beaucoup de terrain.
Je finis par voir une tactique pour revenir dans la partie, mais elle ne fonctionnait pas à cause de Cf6 qui était resté là toute la partie.
Son premier coup et il ne l'aura finalement jamais rebougé.
Je n'ai plus aucune chance, j'abbandonne...

## Bilan

Mauvaise attitude de ma part, tant pis si je suis hors théorie, j'aurais pu me contenter de jouer des coups logiques et de développement faute d'être à l'aise contre l'Alekhine.
C'est une bonne leçon pour moi. C'est une journée à deux défaites pour moi.