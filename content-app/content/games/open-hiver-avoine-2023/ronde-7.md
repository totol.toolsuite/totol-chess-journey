---
type: 'game'
date: '2023-02-25'
place: 'Avoine'
tournament: 'Open Hiver Avoine 2023'
title: 'Ronde 7'
tags: ['Ponziani']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'white'
opponent: 'Yanis HOUX'
opponent_rating: '1387'
description: 'meta description of the page'
result: -1
lichess: https://lichess.org/study/zjP8UWwS/6g92QITY
pgn: '1. e4 e5 2. Nf3 Nc6 3. c3 Nf6 4. d4 d6 5. d5 Ne7 6. Bg5 Ng6 7. Bb5+ Bd7 8. Bxd7+
Qxd7 9. Nbd2 Be7 10. Qb3 b6 11. h3 h6 12. Be3 Nh5 13. O-O-O Nhf4 14. g3 Nxh3 15.
Rh2 O-O-O 16. Rdh1 Ng5 17. Ne1 f5 18. f3 Nf7 19. Kb1 f4 20. gxf4 Nxf4 21. Nc4
Bg5 22. Na3 Ng6 23. Bg1 Bf4 24. Rc2 Ng5 25. Nb5 a6 26. Qa4 Qxb5 *'

---
::GameHeader{title="Belle position d'attaque, mais gros blunder"}
::

## Contexte

Dernière journée de ce tournoi et dernière partie.
J'arrive avec 10 minutes de retard car nous devions ranger le Airbnb, et nous nous sommes couché assez tard.
Mon adversaire n'est pas là mais a lancé la pendule.
Je joue mon premier coup et me lève directement pour aller chercher un café.
Je reviens mon adversaire que je n'ait toujours pas croisé a joué son coup et est reparti aussi.
Je crois que nous nous sommes croisé pour la première fois et serré la main au coup 4.

## La partie

### Ouverture

Je peux enfin jouer la Ponziani, mon adversaire joue une variante peu ambitieuse qui me laisse beaucoup d'espace, mais évite tout piège dans l'ouverture

### Milieu de jeu

Mon adversaire tente de progresser à l'aile roi, alors qu'il n'a toujours pas donné la position de son roque.
Je manoeuvre très bien le milieu de jeu et menace une très belle attaque sur son roque.
Malheureusement, je vois une tactique imaginaire avec 26. Da4, pendant que s'il prend le cavalier avec le pion il y a Da8#.
En réalité il peut tout simplement prendre avec la dame.
Je passe brutalement d'une belle position dominante à une position totalement poubelle.
Dégouté, j'abandonne subitement.

## Bilan

Nous sommes allés analyser la partie et il se trouve que j'avais vraiment une belle position, je suis dégouté.
J'avais encore du temps à la pendule, je ne dois jamais faire une erreur aussi bête, c'est une leçon de plus dans cet Open d'Avoine qui aura été riche en enseignements.
Difficile de terminer comme ça mais je reviendrais plus fort.