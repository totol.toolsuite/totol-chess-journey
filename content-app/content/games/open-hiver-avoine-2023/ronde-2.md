---
type: 'game'
date: '2023-02-22 17:00'
place: 'Avoine'
tournament: 'Open Hiver Avoine 2023'
title: 'Ronde 2'
tags: ['Roque Opposé']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'white'
opponent: 'Jocelyne WOLFANGEL'
opponent_rating: '1184'
description: 'meta description of the page'
result: 1
lichess: https://lichess.org/study/zjP8UWwS/Dh3GQ90D
pgn: '1. e4 d6 2. d4 e6 3. Nf3 Be7 4. Be3 a6 5. Be2 Nf6 6. Nc3 O-O 7. Qd2 c5 8. d5 Ng4
9. Bg5 h6 10. Bxe7 Qxe7 11. h3 Nf6 12. O-O-O e5 13. g4 Nh7 14. Rdg1 g5 15. h4 f6
16. Ne1 Rf7 17. Ng2 Rg7 18. Ne3 b5 19. Nf5 Bxf5 20. gxf5 Nd7 21. hxg5 hxg5 22.
Rh3 Nb6 23. Rgh1 b4 24. Nb1 a5 25. Bh5 c4 26. Bg6 Nf8 27. Rh8# 1-0'

---
::GameHeader{title="Bonne attaque de roques opposés"}
::

## Contexte

La première partie a été longue, et comme la première ronde du jour n'était pas le matin mais à 13h, il est déjà 17h30 au début du match, une partie de 4h comme le matin m'emmenerait tard, je suis donc déterminé à attaquer pour terminer la partie rapidement

Je suis appareillé contre une autre femme, un peu plus agé, et moins bien classé que ce matin.

## La partie

### Ouverture

Son ouverture est très passive, et peu conventionnel. Je suis totalement hors théorie rapidement. et me contente de prendre de l'espace

### Milieu de jeu

Jocelyne ferme le centre et roque assez vite. Je me décide donc à jouer une attaque de roque opposée. C'est un plan de jeu aggressif mais je suis en confiance. Elle perd un peu de temps tandis que j'envois mon attaque le plus vite possible. Le plan de jeu est clair, attaquer son roi avant qu'elle attaque le mien.

### Finale

Je finis par rentrer en forcer sur la colonne 8. Elle rate même le mat sur l'échiquier, mais ma position était totalement gagnante quoi qu'il en soit. Fin de la première journée après 1h30 de jeu seulement cet après midi.

## Bilan

Très bonne partie de mon côté, et très bonne fin de première journée puisque je suis parti des dernières tables, et vais clairement remonter avec ce 1,5 / 2.