---
type: 'game'
date: '2023-02-23 9:00'
place: 'Avoine'
tournament: 'Open Hiver Avoine 2023'
title: 'Ronde 3'
tags: ['Pirc']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'black'
opponent: 'Reinier JAQUET'
opponent_rating: '1930'
description: 'meta description of the page'
result: -1
lichess: https://lichess.org/study/zjP8UWwS/4olSbKrK
pgn: '1. e4 d6 2. Nf3 Nf6 3. c3 g6 4. Bc4 Bg7 5. O-O O-O 6. Qe2 e5 7. d4 Nc6 8. Rd1
Bg4 9. h3 Bxf3 10. Qxf3 Nd7 11. Be3 Na5 12. Bb5 c6 13. Be2 b5 14. b4 Nc4 15.
Bxc4 bxc4 16. d5 c5 17. bxc5 Nxc5 18. Bxc5 dxc5 19. Qe2 f5 20. Qxc4 Qd6 21. Nd2
h6 22. Rab1 Rf7 23. Rb5 Bf8 24. Rdb1 g5 25. Rb7 Qg6 26. Rxf7 Qxf7 27. Qa6 fxe4
28. Nxe4 Kg7 29. Rb7 Be7 30. d6 Rd8 31. Rxe7 1-0'

---
::GameHeader{title="La marche était trop haute"}
::

## Contexte

Après une très bonne première journée, où je suis parti de la table 33/37, et où j'ai terminé à 1,5 / 2, je suis en pleine confiance.
J'arrive à 9h tout pile pour voir les appareillements. Je joue contre un joueur classé 1930, à la table 9/37.
C'est de loin l'adversaire le plus fort que j'ai eu à affronter.
Mon adversaire est un néerlandais très courtois, j'arrive avec 2 minutes de retard à la table, il m'a attendu pour lancer la pendule.
À mon arrivée, il joue son premier coup et me propose d'aller me chercher un café !

## La partie

### Ouverture

1 e4, je suis très content de jouer ma Pirc contre un fort joueur. 1...d6
Il me joue une sorte de système Alapine / Ponziani, surement pour sortir tout de suite de la théorie contre un joueur plus faible que lui.

### Milieu de jeu

En milieu de jeu, j'ai pris une mauvaise décision en me laissant doubler des pions, et j'ai eu beaucoup de mal à tenir. Après avoir concédé un petit avantage, il s'est engoufré dans la brèche et est parvenu à prendre un avantage décisif.
Je ne peux qu'abandonner.

## Bilan

Première défaite en partie officielle, je suis tout d'abord très déçu.
Malgré tout, mon adversaire m'a félicité pour ma partie, et je suis relativement content de ma partie.
Mon ouverture est assez bonne, je manque tout simplement d'experience face à un joueur de ce niveau.