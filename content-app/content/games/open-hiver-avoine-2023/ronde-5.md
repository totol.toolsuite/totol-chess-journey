---
type: 'game'
date: '2023-02-24 9:00'
place: 'Avoine'
tournament: 'Open Hiver Avoine 2023'
title: 'Ronde 5'
tags: ['Pirc', 'Attaque Autrichienne']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'black'
opponent: 'Antoine TUAIRE'
opponent_rating: '1223'
description: 'meta description of the page'
result: 1
lichess: https://lichess.org/study/zjP8UWwS/TR9GebUU
pgn: '1. e4 d6 2. d4 Nf6 3. Nc3 g6 4. f4 Bg7 5. Nf3 c5 6. dxc5 Qa5 7. Bb5+ Bd7 8. Qd3
a6 9. Bxd7+ Nbxd7 10. cxd6 Nc5 11. Qc4 Nfxe4 12. d7+ Nxd7 13. Qxe4 Bxc3+ 14. Bd2
Bxd2+ 15. Nxd2 O-O 16. b4 Qb6 17. Nb3 Rfd8 18. O-O-O Rac8 19. Qxe7 Qc7 20. Na1
a5 21. b5 b6 22. Qd6 Qc3 23. Kb1 a4 24. Qd4 Qa5 25. a3 Qxb5+ 26. Qb4 Qxb4+ 27.
axb4 b5 28. Rd5 Rc4 29. Rxb5 Rxf4 30. c3 Rc8 31. Rc1 Rf2 32. c4 a3 33. Nc2 Rxc4
34. Nxa3 Rxc1+ 35. Kxc1 Rxg2 36. Rb7 Ne5 37. b5 Rxh2 38. Rc7 Ra2 39. Nc4 Nxc4
40. Rxc4 Ra8 41. Kd2 Rb8 42. Rb4 Kf8 43. Ke3 Ke7 44. Kd4 Kd6 45. Kc4 h5 46. b6
Kc6 47. Kd3 Rxb6 48. Rf4 Rb7 49. Rf6+ Kd5 50. Ke3 Ke5 51. Ra6 Kf5 52. Kf3 Re7
53. Ra5+ Re5 54. Ra7 Kf6 55. Ra6+ Kf5 56. Ra7 0-1 '
---

::GameHeader{title="Victoire avec la Pirc dans un rallye de plus de 4h"}
::


## Contexte

Il est temps de renouer avec la victoire, 3ème journée de cet open d'Avoine.
Je joue contre un adversaire très sympa avec qui j'avais déjà parlé dehors la veille sans savoir que nous nous affrontrions.
Il est du même club que Liv que j'ai affronté le premier jour.

## La partie

### Ouverture

Il m'envoi 1. e4, je suis très content et répond avec la Pirc, d6.
Il est du même club que mon adversaire à la ronde 1, et tente la [même attaque](/games/open-hiver-avoine-2023/ronde-1) comme par hasard avec une attaque Autrichienne.
Je répond de la même manière, et sort encore très bien de l'ouverture avec des belles possibilités tactiques.

### Milieu de jeu

J'insiste sur la colonne ouverte dans le milieu de jeu, et finis par prendre une petite initiative.
Mon adversaire me contre dans un premier temps mais finis par subir des échanges qui me sont favorables.
Mon adversaire n'a le choix que de concéder plusieurs pions et de rentrer dans une finale perdante.
Après pas loin de 4h de jeux, et moins de 10 minutes à la pendule chacun, nous rentrons dans une finale avec tour + 3 pions vs tour + 1 pion.


### Finale

Je parviens à obtenir le pion de mon adversaire, qui n'a plus que quelques secondes à la pendule.
J'ai du mal à progresser dans la position avec la fatigue et le stress, et mon adversaire s'accroche.
Je trouve un setup pour avancer sans qu'il puisse me mettre en echecs avec sa tour, et mon adversaire fini par abandonner.
Toutes les autres parties étaient terminées et il y avait plusieurs personnes qui nous regardaient jouer.

## Bilan

Ma partie la plus longue et intense jusqu'ici, je suis très satisfait de mon jeu.
La partie n'est évidemment pas parfaite, mais je me relance, 2,5 /5.
Je n'ai que 30minutes pour manger et être prêt pour une deuxième partie longue...