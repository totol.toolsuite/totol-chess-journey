---
type: 'tournament'
tournament: 'Open Hiver Avoine 2023'
tournamentStartDate: '2023-02-22'
tournamentEndDate: '2023-02-25'
---
# Open d'hiver d'Avoine 2023, mon premier Open FIDE

Décidé à m'investir dans ma progression en cette année 2023, je me rends à Avoine pour l'Open d'hiver.

Je m'y rend avec mon ami d'enfance Paul, qui s'est inscrit en club cette année aussi. C'est notre premier open à tous les deux. Je l'avais motivé à prendre une licence pour qu'on puisse se faire de tournoi.

On s'est pris un Airbnb juste à côté, posé des congés, et c'est l'occasion de passer du temps et de faire cette expérience ensemble.

## Le tournoi

Le tournoi se déroule dans une grande salle, du mercredi au samedi, en 7 rondes.

- Ronde 1 : Mercredi 13h
- Ronde 2 : Mercredi 17h
- Ronde 3 : Jeudi 9h
- Ronde 4 : Jeudi 14h
- Ronde 5 : Vendredi 9h
- Ronde 6 : Vendredi 14h
- Ronde 7 : Samedi 9h

Il y a 74 participants.

La cadence est en 1h30 + 30 secondes par coup.

Les appareillements sont donnés 1h avant chaque match.

C'est un tournoi ouvert sans limite de classement, les classements Elo des joueurs vont de 1000 à 2200.
Je débute le tournoi personnellement avec un classement de 1130, ce qui fait de moi un des joueurs les moins bien classés du tournoi

## Bilan 

Bien que les résultats ne soient pas forcément au rendez-vous, nous sommes tous les deux très content de l'adréline et de l'experience.

J'avais préparé la défense Pirc contre e4 pour le tournoi, et je suis plutôt satisfait même si j'ai du travail

En revanche avec les blancs ça a été plus compliqué, il va falloir travailler pour trouver une ouverture qui me correspond.

J'effectue un bon début de tournoi avec 1,5 / 2 la première journée.
Je me retrouve ensuite à jouers proche des premières tables à jouer un joueur à plus de 1900, et j'ai ensuite enchainé les défaites. Malgré tout, j'ai eu beaucoup d'enseignements et je suis ravis de l'experience, que je referais avec plaisir.