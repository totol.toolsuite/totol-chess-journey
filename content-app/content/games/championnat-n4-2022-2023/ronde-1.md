---
type: 'game'
date: '2022-09-29'
place: 'Rennes'
tournament: 'Interclubs 2022-2023'
title: 'Ronde 1'
tags: ['Alapine']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'white'
opponent: 'Inconnu'
opponent_rating: '1199'
description: 'meta description of the page'
result: 1
lichess: 'https://lichess.org/study/FEnzrgoC/j1YdUOMf'
pgn: '1. e4 c5 2. Nf3 Nc6 3. c3 Nf6 4. e5 Nd5 5. d4 e6 6. Bc4 Nb6 7. Bb5 a6 8. Bxc6
bxc6 9. O-O cxd4 10. cxd4 Be7 11. Nbd2 f5 12. Nb3 O-O 13. Nc5 Nd5 14. a3 a5 15.
Bd2 d6 16. exd6 Bxd6 17. b4 axb4 18. axb4 Rxa1 19. Qxa1 Qb6 20. Rb1 h6 21. Ne5
Bxe5 22. dxe5 Qd8 23. Rb2 f4 24. Qa5 Nb6 25. Ne4 Nc4 26. Qxd8 Rxd8 27. Rc2 Nxe5
28. f3 g5 29. Rc5 Rd5 30. Nf6+ Kf7 31. Nxd5 exd5 32. Bc3 Ke6 33. Bxe5 Kxe5 34.
Rxc6 Be6 35. Rc5 Kd4 36. Kf2 Bf5 37. Rc1 Bd3 38. Rd1 Kc4 39. b5 Bf5 40. b6 1-0'
---
::GameHeader{title="Première partie lente depuis. On s'impose"}
::

## Contexte

Première partie lente pour moi, nous prenons la route avec mon coéquipier Samuel, pour qui c'est la première fois aussi.
Nous appréhendons un petit peu la cadence très lente, mais sommes aussi excités.
## La partie

### Ouverture

Je pensais faire une Ponziani, qui est l'ouverture que je travaillais à ce moment là. J'ai appris plus tard que contre la Sicilienne, ce système s'appellait l'Alapine ...

### Milieu de jeu

Je sors avec une position égale du milieu de jeu, je finis par lacher un pion mais plante une fourchette Roi-Tour à mon adversaire.

### Finale

La conversion technique est assez propre, j'échange suffisamment de pièce et menace de partir à dame, mon adversaire abandonne.
## Bilan

Première victoire encourageante, contre un joueur non classé donc elle ne comptera pas de points elo malheureusement.