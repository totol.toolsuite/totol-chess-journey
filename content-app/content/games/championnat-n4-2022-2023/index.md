---
type: 'tournament'
tournament: 'Interclubs 2022-2023'
tournamentStartDate: '2022-09-01'
tournamentEndDate: '2023-07-31'
---
# Matchs d'interclub de la saison 2022/2023

## Contexte

Après avoir fait mon premier match officiel dans la saison précédente, je souhaite progresser cette année et me mettre à fond à la compétition..

Je suis 1130 en début d'année, mon objectif serait d'atteindre 1300 voir 1400, je pense que je peux jouer à ce niveau là.

Je joue avec le club de Plancoet, il y aura 3 4 ou 5 rondes, selon mes dispos.

Nous avons deux équipe une en D1 et une en N4, les deux équipes jouent la montée.

## Bilan

L'équipe A est passée à 0,5 points de la montée en N3 et l'équipe B est montée en N4 (j'ai joué avec les deux)

Malheureusement, par manque d'effectif, l'équipe B refuse la montée en N4, car notre deux équipes seraient dans la même division, et ne pourraient plus échanger de joueurs, comme nous l'avons fait cette année où les joueurs pouvaient passer de l'une à l'autre.

Cela reste une très bonne saison pour le club qui il y a 1 an n'avait encore qu'une équipe.