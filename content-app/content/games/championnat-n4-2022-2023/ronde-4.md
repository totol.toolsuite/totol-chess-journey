---
type: 'game'
date: '2023-01-29'
place: 'Plancoet'
tournament: 'Interclubs 2022-2023'
title: 'Ronde 4'
tags: ['Pirc']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'black'
opponent: 'Ebeniste de Saint-Brieuc'
opponent_rating: '1199'
description: 'meta description of the page'
result: 1
lichess: 'https://lichess.org/study/FEnzrgoC/GuJkpukT'
pgn: '1. e4 d6 2. d4 Nf6 3. Nc3 g6 4. Be3 Bg7 5. Be2 c6 6. Qd2 b5 7. a3 Nbd7 8. f4 e5
1. dxe5 dxe5 10. Nf3 Ng4 11. O-O Nxe3 12. Qxe3 exf4 13. Qxf4 O-O 14. Rad1 Qb6+
2.  Kh1 Nc5 16. Ne5 Ne6 17. Qg3 Qc7 18. Nd3 Qxg3 19. hxg3 Nd4 20. Rd2 Nxe2 21.
Rxe2 Bg4 22. Ref2 Bd4 23. Rf4 h5 24. Nf2 g5 25. Nxg4 gxf4 26. gxf4 hxg4 27. e5
Kg7 28. Kh2 Bxc3 29. bxc3 f6 30. e6 Rae8 31. f5 Kh6 32. g3 Kg5 33. Kg2 Re7 34.
Rf4 Rd8 35. Rf2 Rd5 *'
---
::GameHeader{title="Victoire facile avec la Pirc"}
::


## Contexte

Match contre Saint-Brieuc, j'ai fait une petite pause de 2 mois dans les échecs, car j'ai été très occupé par mon travail.
Avant ma pause, je commençais à jouer la Pirc et à travailler certaines variante.
Mon adversaire est un homme assez imposant mais classé 1199.

## La partie

### Ouverture

1 e4, je lance ma première Pirc, 1. ...d6
J'ai surtout travaillé contre l'attaque Autrichienne avec d4 e4 f4, et l'attaque 150 avec dame d2 et fou e3 en batterie qui viennent chercher mon Fianchetto, et envoient la marée de pions.

Mon adversaire fait une espèce de mélange des deux, avec la puissance d'aucune des deux.


### Milieu de jeu

Je donne la position de mon roi en roquant le plus tard possible. Je sors très bien de l'ouverture et finis par obtenir des très belles diagonales pour mon fou.
Je domine mon adversaire tactiquement et enferme sa tour.
Avec une pièce de plus, je stabilise la position et il finit par abbandonner.

## Bilan

Belle victoire avec cette Pirc, qui deviendra mon coup de coeur et que je rejouerais pendant tout le tournoi d'Avoine quelques semaines plus tard.