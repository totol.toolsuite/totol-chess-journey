---
type: 'game'
date: '2022-11-12'
place: 'Plancoet'
tournament: 'Interclubs 2022-2023'
title: 'Ronde 2'
tags: ['Black Lion System']
cadence: '1h30 + 30s'
my_rate: '1130'
my_color: 'black'
opponent: 'Alexandre MASSON'
opponent_rating: '1198'
description: 'meta description of the page'
result: 1
lichess: 'https://lichess.org/study/FEnzrgoC/pSwwTEZ7'
pgn: '1. e4 d6 2. d4 Nd7 3. Nf3 e5 4. d5 Ngf6 5. Bd3 Be7 6. c4 Nc5 7. Nc3 c6 8. O-O a5
9. Be3 Nxd3 10. Qxd3 c5 11. Nb5 O-O 12. a4 Ng4 13. Rfe1 Nxe3 14. Rxe3 f5 15. Nd2
f4 16. Re2 Rf6 17. f3 Rg6 18. Kf1 Bh4 19. Nb1 Qg5 20. N1c3 Bd7 21. Ra2 Rf8 22.
b3 Qd8 23. Qc2 Rh6 24. h3 Rff6 25. Rd2 Rfg6 26. Ra1 Qg5 27. Qb2 Qg3 28. Na3 Qh2
29. Ncb5 Bxh3 30. gxh3 Rg1# 0-1'
---
::GameHeader{title="Attaque violente avec la black lion system"}
::

## Contexte

Je joue pour la première fois au club de Plancoet, très content, contre l'équipe de Yffiniac.
Mon adversaire Alexandre est un jeune qui a battu un coequipier de la B il y a 15 jours, on m'avertit de me méfier malgrès sont elo de 1198, il n'a que 11ans et est en pleine progression

## La partie

### Ouverture

Je travaillais avec les noirs à ce moment là un système appellé Black Lion System, qui est une sous variante de la Philidor.
L'objectif est de ramener les deux cavalier côté roi, et d'envoyer les pions.
Je tente ma chance.

### Milieu de jeu

J'obtiens ce que je souhaite de l'ouverture, la position est fermée, et j'ai un plan d'attaque.
Je ramène mes pièces unes à unes à l'aile roi, tandis que mon adversaire ne trouve pas vraiment d'amélioration à sa position
Mon adversaire lassé me propose même nulle au coup 26.
Sans façon, je sais très bien que je vais finir par sacrifier une pièce et rentrer le matter.
C'est ce qu'il finit par se passer, son roi ne peut partir, le mat est imparable.

## Bilan

Très belle victoire avec les noirs. Même si j'ai arreté de jouer cette position au profit de la Pirc.