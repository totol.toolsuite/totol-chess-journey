export interface IMove {
  num: number
  white?: string
  black?: string
}

export type IMoveColor = 'white' | 'black'

export type IPgn = IMove[]