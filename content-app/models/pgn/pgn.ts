import { IMoveColor, IPgn } from "./pgn.type";

const regex = RegExp(/(\d*\.+) (((\w+\+?\#?)|O-O|O-O-O) ){1,2}/g)

export const parsePgn = (pgn: string|undefined): IPgn | undefined => {
  if (!pgn) return
  const matchs = pgn?.match(regex)

  const parsed: IPgn|undefined = matchs?.map(x => {
    const dotSplit = x.split('.')
    const movesSplit = dotSplit[1].slice(1, dotSplit[1].length-1).split(' ')
    return {  
        num: parseInt(dotSplit[0]),
        white: movesSplit[0],
        black: movesSplit[1]
      }
    }
  )

  return parsed
}

// Pourrait-être amélioré avec des regex, mais ça fonctionne bien
export const goToMove = (pgn: string, moveNumber: number, moveColor: IMoveColor) => {
  let slicedPgn = pgn.split(`${moveNumber+1}.`)[0]

  if (moveColor === 'white') {
    const matchs = slicedPgn?.match(regex)
    if (matchs) {
      const slicedItem = matchs[matchs.length-1].split(' ').slice(0, 2).join(' ')
      slicedPgn = [...matchs.slice(0, matchs.length-1), slicedItem].join(' ')
    }
  }
  return slicedPgn
}