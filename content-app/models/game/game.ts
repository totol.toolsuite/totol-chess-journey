import { ParsedContent } from "@nuxt/content/dist/runtime/types";
import { IGame } from "./game.type";
import dayjs from 'dayjs'
export const contentToIGame = (content: ParsedContent): IGame => {
  return {
    type: content.type,
    title: content.title || '',
    opponent: content.opponent,
    result: content.result,
    tournament: content.tournament,
    opponent_rating: content.opponent_rating,
    pgn: content.pgn,
    path: content._path || '',
    date: dayjs(content.date),
    lichess: content.lichess,
    my_color: content.my_color,
    my_rate: content.my_rate,
    place: content.place,
    tags: content.tags,
    cadence: content.cadence,
    tournamentStartDate: content.tournamentStartDate,
    tournamentEndDate: content.tournamentEndDate
  }
}