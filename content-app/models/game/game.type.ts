import type { Dayjs } from "dayjs"
import { IMoveColor } from "../pgn/pgn.type"
import { PageType } from "../types"

export interface IGame {
  type: PageType
  title: string
  opponent: string
  opponent_rating: number
  tournament: string
  my_rate: number
  my_color: IMoveColor
  result: number
  pgn: string
  path: string
  place: string
  tags: string[]
  date: Dayjs
  lichess?: string
  cadence: string
  tournamentStartDate?: Dayjs
  tournamentEndDate?: Dayjs
}