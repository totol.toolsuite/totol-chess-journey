# totol-chess-journey

The goal of this project is to track my official chess games, in order to follow my progression in the next years

## Stack

- The idea is to use nuxt content to add new games in the projects
- D3js to add sexy graphics
- Build my own chess board to analyze game, or to find a js librarie doing it 
- Use some API to have chess analysis, or to host a server doing it if I don't find a free alternative